
var maiTutor = angular.module('starter', ['ionic', 'ngCordova', 'gm', 'LocalStorageModule', 'angular.filter'])

maiTutor.run(function($ionicPlatform, localStorageService, $rootScope, $state) {
  $ionicPlatform.ready(function() {
    // $rootScope.loggedInUser = localStorageService.get('currentUser')
    // if($rootScope.loggedInUser){
    //   $state.go('parentHome')
    // }
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
      window.FirebasePlugin.setBadgeNumber(0);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    
  });
})

maiTutor.constant('URL', {
  baseDomain  :'http://www.maitutor.com/api/',
  imgUrl      : 'http://www.maitutor.com/uploads/profile/'
  // baseDomain:'http://myxlabdevelopment.com/maitutor/api/',  
  // imgUrl : 'http://myxlabdevelopment.com/maitutor/uploads/profile/'
})

