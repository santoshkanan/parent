maiTutor.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/landing');
		
	$stateProvider
		.state('landingLayoutwithoutlogin', {
			abstract 	: true,
			views			: {
				layout: {
					templateUrl: 'template/layout/sidemenuwithoutlogin.html',
				}
			},
			controller : 'sideMenuController'
		})
			.state('landing', {
				url   		: '/landing',
				templateUrl	: 'template/landing/landing.html',
				controller 	: 'authController',
				parent		: 'landingLayoutwithoutlogin',
			})
			.state('goodfit', {
				url   		: '/goodfit',
				templateUrl	: 'template/findTutor/goodfit.html',
				controller 	: 'authController',
				parent		: 'landingLayoutwithoutlogin',
			})
			
		.state('landingLayout', {
			abstract 	: true,
			views			: {
				layout: {
					templateUrl: 'template/layout/landingLayout.html',
				}
			}
		})
			// LOGIN VIEW ===========================================
			.state('signuplogin', {
				url   			: '/signuplogin',
				templateUrl	: 'template/landing/signuplogin.html',
				controller 	: 'authController',
				parent			: 'landingLayout',
			})

		.state('default', {
			abstract 	: true,
			views			: {
				layout: {
					templateUrl: 'template/layout/defaultLayout.html',
				}
			}
		})
		.state('links', {
			url   		: '/links',
			templateUrl	: 'template/auth/links.html',
			parent		: 'default',
		})
			
			// LOGIN VIEW ===========================================
			.state('hiw', {
				url   			: '/hiw',
				templateUrl	: 'template/first/hiw.html',
				controller 	: 'authController',
				parent			: 'default',
			})// LOGIN VIEW ===========================================
			.state('faq', {
				url   			: '/faq',
				templateUrl	: 'template/first/faq.html',
				controller 	: 'authController',
				parent			: 'default',
			})
			.state('setting', {
				url   			: '/setting',
				templateUrl	: 'template/home/setting.html',
				controller 	: 'sideMenuController',
				parent			: 'sidemenu',
			})
			.state('setting1', {
				url   			: '/setting1',
				templateUrl	: 'template/home/setting1.html',
				controller 	: 'sideMenuController',
				parent			: 'default',
			})
			.state('acceptedRequest', {
				url   			: '/acceptedRequest',
				templateUrl	: 'template/mytutor/accepted_request.html',
				controller 	: 'scheduleController',
				parent			: 'default',
			})
			.state('paymentbreakdown', {
				url   			: '/paymentbreakdown',
				templateUrl	: 'template/mytutor/paymentbreakdown.html',
				controller 	: 'scheduleController',
				parent			: 'default',
			})
			.state('addtimetable', {
				url   			: '/addtimetable',
				templateUrl	: 'template/mytutor/addtimetable.html',
				controller 	: 'scheduleController',
				parent			: 'default',
			})
			.state('msgtutor', {
				url   			: '/msgtutor',
				templateUrl	: 'template/mytutor/msgtutor.html',
				controller 	: 'scheduleController',
				parent			: 'default',
			})
			.state('writemsg', {
				url   			: '/writemsg',
				templateUrl	: 'template/mytutor/writemsg.html',
				controller 	: 'scheduleController',
				parent			: 'default',
			})
			.state('bookingsummary', {
				url   			: '/bookingsummary',
				templateUrl	: 'template/mytutor/bookingsummary.html',
				controller 	: 'scheduleController',
				parent			: 'default',
			})
			.state('banklist', {
				url   			: '/banklist',
				templateUrl	: 'template/mytutor/banklist.html',
				controller 	: 'scheduleController',
				parent			: 'default',
			})
			.state('reviewpage', {
				url   			: '/reviewpage',
				templateUrl	: 'template/mytutor/reviewpage.html',
				controller 	: 'scheduleController',
				parent			: 'cross',
			})
			
			.state('uploadPic', {
				url   			: '/uploadPic',
				templateUrl	: 'template/home/uploadPic.html',
				controller 	: 'sideMenuController',
				parent			: 'sidemenu',
			})
			.state('changePassword', {
				url   			: '/changePassword',
				templateUrl	: 'template/home/changePassword.html',
				controller 	: 'sideMenuController',
				parent			: 'sidemenu',
			})
			.state('changeEmail', {
				url   			: '/changeEmail',
				templateUrl	: 'template/home/changeEmail.html',
				controller 	: 'sideMenuController',
				parent			: 'sidemenu',
			})


			// LOGIN VIEW ===========================================
			.state('login', {
				url   			: '/login',
				templateUrl	: 'template/auth/login.html',
				controller 	: 'authController',
				parent			: 'default',
			})
			
			// SIGNUP VIEW ==========================================
			.state('signup', {
				url 				: '/signup',
				templateUrl	: 'template/auth/signup.html',
				controller 	: 'authController',
				parent			: 'default',       
			})
			// SIGNUP VIEW ==========================================
			.state('signup1', {
				url 				: '/signup1',
				templateUrl	: 'template/auth/signup1.html',
				controller 	: 'authController',
				parent			: 'default',       
			})
			// SIGNUP VIEW ==========================================
			.state('signup2', {
				url 				: '/signup2',
				templateUrl	: 'template/auth/signup2.html',
				controller 	: 'authController',
				parent			: 'default',       
			})
			// SIGNUP VIEW ==========================================
			.state('signup3', {
				url 				: '/signup3',
				templateUrl	: 'template/auth/signup3.html',
				controller 	: 'authController',
				parent			: 'default',       
			})
			// SIGNUP VIEW ==========================================
			.state('signup4', {
				url 				: '/signup4',
				templateUrl	: 'template/auth/signup4.html',
				controller 	: 'authController',
				parent			: 'default',       
			})
			// SIGNUP VIEW ==========================================
			.state('signup5', {
				url 				: '/signup5',
				templateUrl	: 'template/auth/signup5.html',
				controller 	: 'authController',
				parent			: 'default',       
			})
			.state('signup6', {
				url 				: '/signup6',
				templateUrl	: 'template/auth/signup6.html',
				controller 	: 'authController',
				parent			: 'default',       
			})

			// FORGET PASSWORD VIEW =================================
			.state('forgotPassword', {
				url 				: '/forgotPassword',
				templateUrl : 'template/auth/forgotPassword.html',
				controller 	: 'authController',
				parent			: 'default',       
			})
		.state('wihoutBar', {
			abstract 	: true,
			views			: {
				layout: {
					templateUrl: 'template/layout/wihoutbar.html',
				}
			}
		})
						// STUDENT HOMEPAGE VIEW ================================
			.state('findTutor1', {
				url 		: '/findTutor1',
				templateUrl	: 'template/findTutor/find-tutor-input.html',
				controller 	: 'searchController',
				parent 			: 'wihoutBar'       
			})

			// TEACHER HOMEPAGE VIEW ================================
			.state('findTutor2', {
				url 		: '/findTutor2',
				templateUrl : 'template/findTutor/find-tutor-input2.html',
				controller 	: 'searchController',
				parent  		: 'wihoutBar'       
			})
			
			// PROFILE VIEW =========================================
			.state('findTutor3', {
				url 		: '/findTutor3',
				templateUrl	: 'template/findTutor/find-tutor-input3.html',
				controller 	: 'searchController',
				parent 			: 'wihoutBar'       
			})

			// SEARCHED PROFILE VIEW ================================
			.state('findTutor4', {
				url 		: '/findTutor4',
				templateUrl	: 'template/findTutor/find-tutor-input4.html',
				controller 	: 'searchController',
				parent 			: 'wihoutBar'       
			})
			
			// PROFILE DETAIL VIEW ==================================
			.state('findTutor5', {
				url 		: '/findTutor5',
				templateUrl	: 'template/findTutor/find-tutor-input5.html',
				controller 	: 'searchController',
				parent 			: 'wihoutBar'       
			})

			// PROFILE DETAIL VIEW ==================================
			.state('findTutor6', {
				url 		: '/findTutor6',
				templateUrl	: 'template/findTutor/find-tutor-input6.html',
				controller 	: 'searchController',
				parent 			: 'wihoutBar'       
			})
			// CHANGE PASSWORD VIEW =================================
			.state('tutorlist',{
				url 				: '/tutorlist',
				templateUrl	: 'template/findTutor/tutorsList.html',
				controller  : 'searchController',
				parent 			: 'wihoutBar'
			})
			// CHANGE PASSWORD VIEW =================================
			.state('editsearch',{
				url 				: '/editsearch',
				templateUrl	: 'template/findTutor/editsearch.html',
				controller  : 'searchController',
				parent 			: 'wihoutBar'
			})
			//ADD COURSES CHAT VIEW =================================
			.state('tutorDetail',{
				url 				: '/tutorDetail',
				templateUrl : 'template/findTutor/tutorDetail.html',
				controller 	: 'searchController',
				parent 			: 'wihoutBar'
			})
			.state('oneStepAway', {
				url 				: '/oneStepAway',
				templateUrl	: 'template/findTutor/oneStepAway.html',
				controller 	: 'searchController',
				parent			: 'wihoutBar'
			})
			//RATE TEACHER VIEW =====================================
			.state('addPicture',{
				url 				: '/addPicture',
				templateUrl	: 'template/findTutor/addPicture.html',
				controller 	: 'searchController',
				parent 			: 'wihoutBar'
			})
			.state('steptwo',{
				url 				: '/steptwo',
				templateUrl	: 'template/findTutor/steptwo.html',
				controller 	: 'searchController',
				parent 			: 'no'
			})
			//RATE STUDENT VIEW =====================================
			.state('studentInformation',{
				url 				: '/studentInformation',
				templateUrl	: 'template/findTutor/studentInformation.html',
				controller 	: 'searchController',
				parent			: 'wihoutBar'
			})
		// SIDEMENU VIEW ==========================================
		.state('topmenu', {
			abstract 	: true,
			views 		: {
							layout: {
								templateUrl: 'template/layout/topmenu.html',
							}
						},
		})
			.state('editstudentname',{
				url 				: '/editstudentname',
				templateUrl	: 'template/findTutor/editstudentname.html',
				controller 	: 'searchController',
				parent			: 'topmenu'
			})
			.state('editstudentage',{
				url 				: '/editstudentage',
				templateUrl	: 'template/findTutor/editstudentage.html',
				controller 	: 'searchController',
				parent			: 'topmenu'
			})
			.state('editstudentaddress',{
				url 				: '/editstudentaddress',
				templateUrl	: 'template/findTutor/editstudentaddress.html',
				controller 	: 'searchController',
				parent			: 'topmenu'
			})	
		// SIDEMENU VIEW ==========================================
		.state('sidemenu', {
			abstract 	: true,
			views 		: {
							layout: {
								templateUrl: 'template/layout/sidemenuLayout.html',
							}
						},
			controller : 'sideMenuController'
		})
		.state('sidemenublack', {
			abstract 	: true,
			views 		: {
							layout: {
								templateUrl: 'template/layout/sidemenublack.html',
							}
						},
			controller : 'sideMenuController'
		})
		.state('cross', {
			abstract 	: true,
			views 		: {
							layout: {
								templateUrl: 'template/layout/cross.html',
							}
						},
			controller : 'sideMenuController'
		})
			
			.state('readmore', {
				url 				: '/readmore',
				templateUrl	: 'template/findTutor/readmore.html',
				controller 	: 'searchController',
				parent 			: 'cross'       
			})
			// PROFILE VIEW =========================================
			.state('inviteFrnd', {
				url 				: '/inviteFrnd',
				templateUrl	: 'template/home/inviteFrnd.html',
				controller 	: 'sideMenuController',
				parent 			: 'sidemenu'       
			})
			// PROFILE VIEW =========================================
			.state('parentHome', {
				url 				: '/parentHome',
				templateUrl	: 'template/home/parentHome.html',
				// controller 	: 'userController',
				parent 			: 'sidemenu'       
			})
			.state('reportcard', {
				url 				: '/reportcard',
				templateUrl	: 'template/reportcard/studentname.html',
				controller 	: 'reviewController',
				parent 			: 'sidemenublack'       
			})
			.state('reportcardsubject', {
				url 				: '/reportcardsubject',
				templateUrl	: 'template/reportcard/studentsubject.html',
				controller 	: 'reviewController',
				parent 			: 'sidemenublack'       
			})
			.state('reportcardsession', {
				url 				: '/reportcardsession',
				templateUrl	: 'template/reportcard/studentsession.html',
				controller 	: 'reviewController',
				parent 			: 'sidemenublack'       
			})
			.state('viewreport', {
				url 				: '/viewreport',
				templateUrl	: 'template/reportcard/viewreport.html',
				controller 	: 'reviewController',
				parent 			: 'sidemenublack'       
			})
			
			//CHAT VIEW =============================================
			.state('chatList',{
				url 				: '/chatList',
				templateUrl	: 'template/chat/chatlist.html',
				controller 	: 'chatController',
				parent			: 'sidemenublack'
			})
			.state('chatpage',{
				url 				: '/chatpage',
				templateUrl	: 'template/chat/chatpage.html',
				controller 	: 'chatController',
				parent			: 'cross'
			})
			
			//RATE STUDENT VIEW =====================================
			.state('schedule',{
				url 				: '/schedule',
				templateUrl	: 'template/schedule/schedulelist.html',
				controller 	: 'scheduleController',
				parent			: 'sidemenu'
			})
			.state('scheduledetails',{
				url 				: '/scheduledetails',
				templateUrl	: 'template/schedule/scheduledetails.html',
				controller 	: 'scheduleController',
				parent			: 'sidemenu'
			})
			//RATE STUDENT VIEW =====================================
			.state('myTutor',{
				url 				: '/myTutor',
				templateUrl	: 'template/mytutor/mytutorlist.html',
				controller 	: 'scheduleController',
				parent			: 'sidemenublack'
			})
			//RATE STUDENT VIEW =====================================
			.state('myTutorDetail',{
				url 				: '/myTutorDetail',
				templateUrl	: 'template/mytutor/mytutordeatil.html',
				controller 	: 'scheduleController',
				parent			: 'default'
			})
			//RATE STUDENT VIEW =====================================
			.state('notifications',{
				url 				: '/notifications',
				templateUrl	: 'template/home/notifications.html',
				controller 	: 'sideMenuController',
				parent			: 'sidemenublack'
			})
			
			
		// 	//SCHEDULE CLASS VIEW ===================================
		// 	.state('scheduleClass',{
		// 		cache 			: false,
		// 		url 				: '/scheduleClass',
		// 		templateUrl	: 'template/user/scheduleClass.html',
		// 		controller 	: 'userController',
		// 		parent 			: 'sidemenu'
		// 	})
})