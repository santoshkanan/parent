maiTutor.controller('sideMenuController', function($scope, $rootScope, $http, $state, $ionicLoading, URL, $cordovaCamera, $cordovaImagePicker, $cordovaSocialSharing){
	if($scope.usermodify == undefined){
		$scope.usermodify = {}
	}
	$scope.goto = function(){
		$state.go('signuplogin')
	}

	$scope.findTutor = function(){
		$state.go('findTutor1')
	}

	$rootScope.getnotification = function(){
		$ionicLoading.show();
		var data = {
			"request": 'maitutor/parent_notification',
 			"authkey": $rootScope.loggedInUser.authkey,
 			"data": {
  				"user_email": $rootScope.loggedInUser.user.email
 			}
		}
		$http.post(URL.baseDomain + "maitutor/parent_notification", data)
		.success(function(success){
			$ionicLoading.hide();
			console.log(success)
			$scope.notifications = success.data
			// console.log($scope.notifications)
			$rootScope.unReadNotification = []
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}

	$scope.logout = function(){
		var data = {
			request: 'user/logout',
			authkey: $rootScope.loggedInUser.authkey
		}
		$http.post(URL.baseDomain + "user/logout", data)
		.success(function(success){
			$ionicLoading.hide();
			console.log(success)
			$state.go('login')
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}

	$scope.getChatList = function(){
		var data = {
			"request": 'chat/conversations',
			"authkey": $rootScope.loggedInUser.authkey,
			"data": {
				"uid": "parent@plexsol.com-tutor@plexsol.com",
				"page": 1,
				"limit": 5
			}
		}
		$http.post(URL.baseDomain + "chat/conversations", data)
		.success(function(success){
			$ionicLoading.hide();
			console.log(success)
			$state.go('login')
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}

	$scope.submitEmail = function(){
		$ionicLoading.show()
		var data = {
			"request": 'maitutor/user_edit_email',
			"authkey": $rootScope.loggedInUser.authkey,
			"data": {
				"user_old_email" : $rootScope.loggedInUser.user.email,
  				"user_new_email" : $scope.usermodify.email
			}
		}
		$http.post(URL.baseDomain + "maitutor/user_edit_email", data)
		.success(function(success){
			$ionicLoading.hide();
			console.log(success)
			$state.go('login')
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}
	
	$scope.emptyPopup = function(){
		alert('Please Login')
		$state.go('login')
	}

	$scope.submitPassword = function(){
		$ionicLoading.show()
		var data = {
			"request": 'maitutor/user_change_password',
			"authkey": $rootScope.loggedInUser.authkey,
			"data": {
				"email" : $rootScope.loggedInUser.user.email,
  				"old_password" : $scope.usermodify.oldPassword,
  				"new_password" : $scope.usermodify.newPassword
			}
		}
		$http.post(URL.baseDomain + "maitutor/user_change_password", data)
		.success(function(success){
			$ionicLoading.hide();
			console.log(success)
			if(success.error){
				alert(success.reason)
			}
			else{
				$state.go('login')	
			}
			
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}

	$rootScope.parentPicUpload = function(){
		var options = {
	      destinationType: Camera.DestinationType.FILE_URI,
	      sourceType: Camera.PictureSourceType.CAMERA,
	      allowEdit: true,
	      correctOrientation:true
	    }
	    $cordovaCamera.getPicture(options).then(function(imageURI) {
	      // var image = document.getElementById('myImage');
	      $rootScope.loggedInUser.parentImage = imageURI;
	      $state.go('setting')
	    }, function(err) {
	      alert('Failed because: ' + err);
	    });
	}

	if($rootScope.loggedInUser.parentImage == undefined){
		$rootScope.loggedInUser.parentImage = 'img/profile-pic.jpg'
	}
	
	$rootScope.parentPicGalary = function(){
		var options = {
			maximumImagesCount: 1,
			width: 800,
			height: 800,
			quality: 80
		};

		$cordovaImagePicker.getPictures(options)
			.then(function (results) {
				$rootScope.loggedInUser.parentImage = results[0]
				$state.go('setting')
			}, function(error) {
				alert(error)
			});
	}

	$scope.facebook = function(){
		var message = 'Hi I am using MaiTutor'
		var image = ''
		var link = 'www.google.com'
		$cordovaSocialSharing
    	.shareViaFacebook(message, image, link)
	    .then(function(result) {
	    }, function(err) {
	    });
	}
	$scope.watsapp = function(){
		var message = 'Hi I am using MaiTutor'
		var image = ''
		var link = 'www.google.com'
		$cordovaSocialSharing
    	.shareViaWhatsApp(message, image, link)
	    .then(function(result) {
	    }, function(err) {
	    });
	}
})