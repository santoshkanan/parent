maiTutor.controller("chatController", function($scope, $rootScope, $http, $state, $ionicLoading, URL){
	if($scope.chatlist == undefined){
		$scope.chatlist = {}
	}
	
	if($rootScope.chats == undefined){
		$rootScope.chats = []
	}
	
	if($rootScope.msg == undefined){
		$rootScope.msg = {}
	}
	
	$scope.getChatList = function(){
		$ionicLoading.show();
		var data = {
			"request" : "chat/chat_list",
 			"authkey" : $rootScope.loggedInUser.authkey,
			"data" : {
	  			"from_email" : $rootScope.loggedInUser.user.email,
		  		"status": "Hired"
 			}
		}
		$http.post(URL.baseDomain + "chat/chat_list", data)
		.success(function(success){
			$ionicLoading.hide();
			$scope.chatlist = success.data.list
			console.log($scope.chatlist)
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}

	$scope.getChatDetail = function(from,to){
		$rootScope.to = to
		var data = {
 			"request": 'chat/get_chat_list',
 			"authkey": $rootScope.loggedInUser.authkey,
 			"data": {
  				"from_email": from,
  				"to_email": to
 			}
		}
		setInterval(function(){
			$http.post(URL.baseDomain + "chat/get_chat_list", data)
			.success(function(success){
				console.log(success)
				$ionicLoading.hide();
				$scope.$apply(function() {
					$rootScope.chats = success.data.list
					$rootScope.fromemail = $rootScope.chats[0].to_email
					$rootScope.fromimg = URL.imgUrl+$rootScope.chats[0].img
				})
			})
			.error(function(error){
				$ionicLoading.hide();
				console.log(error)
			})
		},3000)
		$state.go('chatpage')
	}

	$scope.send = function(){
		var senData = {
 			"request": 'chat/send_user_chat',
 			"authkey": "shafiqriz@hotmail.com#87277980c108dd1c01670c2f0a44f56b4fc88d9d",
 			"data": {
  				"from_email": $rootScope.loggedInUser.user.email,
  				"to_email": $rootScope.fromemail,
  				"message" : $rootScope.msg.value
 			}
		}
		$http.post(URL.baseDomain + "chat/send_user_chat", senData)
		.success(function(success){
			console.log(success)
			$rootScope.msg.value = ''
			$scope.getChatDetail($rootScope.loggedInUser.user.email, $rootScope.fromemail)
		})
		.error(function(error){
			console.log(error)
		})
	}
})
