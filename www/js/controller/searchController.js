maiTutor.controller('searchController', function($scope, $rootScope, $state, $http, $ionicLoading, URL, $cordovaCamera, $cordovaImagePicker, $cordovaDialogs, $location, $ionicPopup, $timeout) {
	// $scope.student = {}
	$scope.hidestudentinfo = false;
	if($rootScope.selectedTutorsProfile == undefined){
		$rootScope.selectedTutorsProfile = {}
	}
	if($scope.visible == undefined){
		$scope.visible = true
	}
	if($rootScope.student == undefined){
		$rootScope.student = {}
	}
	
	$scope.setvalue = function(){
		$scope.visible = false
	}
	
	$scope.checksize = function(){
		if($rootScope.searchedTutor.city.length == 0){
			$scope.visible = true	
		}
	}

	if ($rootScope.searchedTutor == undefined) {
		$rootScope.searchedTutor = {}
	}

	$scope.standard1 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidstandard1 = 'inactive-check'

		} else {
			$scope.testidstandard1 = 'active-check'
			$scope.testidstandard2 = 'inactive-check'
			$scope.testidstandard3 = 'inactive-check'
			$scope.testidstandard4 = 'inactive-check'
			$scope.testidstandard5 = 'inactive-check'
			$scope.testidstandard6 = 'inactive-check'
			$rootScope.searchedTutor.grade = 'Std 1-3'
		}
	}

	$scope.standard2 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidstandard2 = 'inactive-check'

		} else {
			$scope.testidstandard1 = 'inactive-check'
			$scope.testidstandard2 = 'active-check'
			$scope.testidstandard3 = 'inactive-check'
			$scope.testidstandard4 = 'inactive-check'
			$scope.testidstandard5 = 'inactive-check'
			$scope.testidstandard6 = 'inactive-check'
			$rootScope.searchedTutor.grade = 'Std 4-6 (UPSR)'
		}
	}

	$scope.standard3 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidstandard3 = 'inactive-check'

		} else {
			$scope.testidstandard1 = 'inactive-check'
			$scope.testidstandard2 = 'inactive-check'
			$scope.testidstandard3 = 'active-check'
			$scope.testidstandard4 = 'inactive-check'
			$scope.testidstandard5 = 'inactive-check'
			$scope.testidstandard6 = 'inactive-check'
			$rootScope.searchedTutor.grade = 'Form 1-3 (PT3)'
		}
	}

	$scope.standard4 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidstandard4 = 'inactive-check'

		} else {
			$scope.testidstandard1 = 'inactive-check'
			$scope.testidstandard2 = 'inactive-check'
			$scope.testidstandard3 = 'inactive-check'
			$scope.testidstandard4 = 'active-check'
			$scope.testidstandard5 = 'inactive-check'
			$scope.testidstandard6 = 'inactive-check'
			$rootScope.searchedTutor.grade = 'Form 4-5 (SPM)'
		}
	}

	$scope.standard5 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidstandard5 = 'inactive-check'

		} else {
			$scope.testidstandard1 = 'inactive-check'
			$scope.testidstandard2 = 'inactive-check'
			$scope.testidstandard3 = 'inactive-check'
			$scope.testidstandard4 = 'inactive-check'
			$scope.testidstandard5 = 'active-check'
			$scope.testidstandard6 = 'inactive-check'
			$rootScope.searchedTutor.grade = 'O-Level/ IGCSE'
		}
	}

	$scope.standard6 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidstandard6 = 'inactive-check'

		} else {
			$scope.testidstandard1 = 'inactive-check'
			$scope.testidstandard2 = 'inactive-check'
			$scope.testidstandard3 = 'inactive-check'
			$scope.testidstandard4 = 'inactive-check'
			$scope.testidstandard5 = 'inactive-check'
			$scope.testidstandard6 = 'active-check'
			$rootScope.searchedTutor.grade = 'A-Level/ Pre-U'
		}
	}

	$scope.subject1 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidsubject1 = 'inactive-check'

		} else {
			$scope.testidsubject1 = 'active-check'
			$scope.testidsubject2 = 'inactive-check'
			$scope.testidsubject3 = 'inactive-check'
			$scope.testidsubject4 = 'inactive-check'
			$scope.testidsubject5 = 'inactive-check'
			$scope.testidsubject6 = 'inactive-check'
			$scope.testidsubject7 = 'inactive-check'
			$scope.testidsubject8 = 'inactive-check'
			$scope.testidsubject9= 'inactive-check'
			$scope.testidsubject10 = 'inactive-check'
			$rootScope.searchedTutor.subject = 'Mathematics'
		}
	}

	$scope.subject2 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidsubject2 = 'inactive-check'

		} else {
			$scope.testidsubject1 = 'inactive-check'
			$scope.testidsubject2 = 'active-check'
			$scope.testidsubject3 = 'inactive-check'
			$scope.testidsubject4 = 'inactive-check'
			$scope.testidsubject5 = 'inactive-check'
			$scope.testidsubject6 = 'inactive-check'
			$scope.testidsubject7 = 'inactive-check'
			$scope.testidsubject8 = 'inactive-check'
			$scope.testidsubject9 = 'inactive-check'
			$scope.testidsubject10 = 'inactive-check'
			$rootScope.searchedTutor.subject = 'Science'
		}
	}

	$scope.subject3 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidsubject3 = 'inactive-check'

		} else {
			$scope.testidsubject1 = 'inactive-check'
			$scope.testidsubject2 = 'inactive-check'
			$scope.testidsubject3 = 'active-check'
			$scope.testidsubject4 = 'inactive-check'
			$scope.testidsubject5 = 'inactive-check'
			$scope.testidsubject6 = 'inactive-check'
			$scope.testidsubject7 = 'inactive-check'
			$scope.testidsubject8 = 'inactive-check'
			$scope.testidsubject9 = 'inactive-check'
			$scope.testidsubject10 = 'inactive-check'
			$rootScope.searchedTutor.subject = 'Bahasa'
		}
	}

	$scope.subject4 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidsubject4 = 'inactive-check'

		} else {
			$scope.testidsubject1 = 'inactive-check'
			$scope.testidsubject2 = 'inactive-check'
			$scope.testidsubject3 = 'inactive-check'
			$scope.testidsubject4 = 'active-check'
			$scope.testidsubject5 = 'inactive-check'
			$scope.testidsubject6 = 'inactive-check'
			$scope.testidsubject7 = 'inactive-check'
			$scope.testidsubject8 = 'inactive-check'
			$scope.testidsubject9 = 'inactive-check'
			$scope.testidsubject10 = 'inactive-check'
			$rootScope.searchedTutor.subject = 'English'
		}
	}

	$scope.subject5 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidsubject5 = 'inactive-check'

		} else {
			$scope.testidsubject1 = 'inactive-check'
			$scope.testidsubject2 = 'inactive-check'
			$scope.testidsubject3 = 'inactive-check'
			$scope.testidsubject4 = 'inactive-check'
			$scope.testidsubject5 = 'active-check'
			$scope.testidsubject6 = 'inactive-check'
			$scope.testidsubject7 = 'inactive-check'
			$scope.testidsubject8 = 'inactive-check'
			$scope.testidsubject9 = 'inactive-check'
			$scope.testidsubject10 = 'inactive-check'
			$rootScope.searchedTutor.subject = 'Geography'
		}
	}

	$scope.subject6 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidsubject6 = 'inactive-check'

		} else {
			$scope.testidsubject1 = 'inactive-check'
			$scope.testidsubject2 = 'inactive-check'
			$scope.testidsubject3 = 'inactive-check'
			$scope.testidsubject4 = 'inactive-check'
			$scope.testidsubject5 = 'inactive-check'
			$scope.testidsubject6 = 'active-check'
			$scope.testidsubject7 = 'inactive-check'
			$scope.testidsubject8 = 'inactive-check'
			$scope.testidsubject9 = 'inactive-check'
			$scope.testidsubject10 = 'inactive-check'
			$rootScope.searchedTutor.subject = 'History'
		}
	}

	$scope.subject7 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidsubject7 = 'inactive-check'

		} else {
			$scope.testidsubject1 = 'inactive-check'
			$scope.testidsubject2 = 'inactive-check'
			$scope.testidsubject3 = 'inactive-check'
			$scope.testidsubject4 = 'inactive-check'
			$scope.testidsubject5 = 'inactive-check'
			$scope.testidsubject6 = 'inactive-check'
			$scope.testidsubject7 = 'active-check'
			$scope.testidsubject8 = 'inactive-check'
			$scope.testidsubject9 = 'inactive-check'
			$scope.testidsubject10 = 'inactive-check'
			$rootScope.searchedTutor.subject = 'Kemahiran'
		}
	}

	$scope.subject8 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidsubject8 = 'inactive-check'

		} else {
			$scope.testidsubject1 = 'inactive-check'
			$scope.testidsubject2 = 'inactive-check'
			$scope.testidsubject3 = 'inactive-check'
			$scope.testidsubject4 = 'inactive-check'
			$scope.testidsubject5 = 'inactive-check'
			$scope.testidsubject6 = 'inactive-check'
			$scope.testidsubject7 = 'inactive-check'
			$scope.testidsubject8 = 'active-check'
			$scope.testidsubject9 = 'inactive-check'
			$scope.testidsubject10 = 'inactive-check'
			$rootScope.searchedTutor.subject = 'Mathematics'
		}
	}
	$scope.subject9 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidsubject9 = 'inactive-check'

		} else {
			$scope.testidsubject1 = 'inactive-check'
			$scope.testidsubject2 = 'inactive-check'
			$scope.testidsubject3 = 'inactive-check'
			$scope.testidsubject4 = 'inactive-check'
			$scope.testidsubject5 = 'inactive-check'
			$scope.testidsubject6 = 'inactive-check'
			$scope.testidsubject7 = 'inactive-check'
			$scope.testidsubject8 = 'inactive-check'
			$scope.testidsubject9 = 'active-check'
			$scope.testidsubject10 = 'inactive-check'
			$rootScope.searchedTutor.subject = 'Lukisan kejurutera'
		}
	}
	$scope.subject10 = function(classname) {
		alert(123)
		if (classname == 'active-check') {
			$scope.testidsubject10 = 'inactive-check'

		} else {
			$scope.testidsubject1 = 'inactive-check'
			$scope.testidsubject2 = 'inactive-check'
			$scope.testidsubject3 = 'inactive-check'
			$scope.testidsubject4 = 'inactive-check'
			$scope.testidsubject5 = 'inactive-check'
			$scope.testidsubject6 = 'inactive-check'
			$scope.testidsubject7 = 'inactive-check'
			$scope.testidsubject8 = 'inactive-check'
			$scope.testidsubject9 = 'inactive-check'
			$scope.testidsubject10 = 'active-check'
			$rootScope.searchedTutor.subject = 'Bahasa Arab'
		}
	}

	$scope.hour1 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidhour1 = 'inactive-check'

		} else {
			$scope.testidhour1 = 'active-check'
			$scope.testidhour2 = 'inactive-check'
			$scope.testidhour3 = 'inactive-check'
			$scope.testidhour4 = 'inactive-check'

			$rootScope.searchedTutor.hrs = '1 Hour'
		}
	}

	$scope.hour2 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidhour2 = 'inactive-check'

		} else {
			$scope.testidhour1 = 'inactive-check'
			$scope.testidhour2 = 'active-check'
			$scope.testidhour3 = 'inactive-check'
			$scope.testidhour4 = 'inactive-check'

			$rootScope.searchedTutor.hrs = '1.5 Hour'
		}
	}

	$scope.hour3 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidhour3 = 'inactive-check'

		} else {
			$scope.testidhour1 = 'inactive-check'
			$scope.testidhour2 = 'inactive-check'
			$scope.testidhour3 = 'active-check'
			$scope.testidhour4 = 'inactive-check'

			$rootScope.searchedTutor.hrs = '2 Hour'
		}
	}

	$scope.hour4 = function(classname) {
		if (classname == 'active-check') {
			$scope.testidhour4 = 'inactive-check'

		} else {
			$scope.testidhour1 = 'inactive-check'
			$scope.testidhour2 = 'inactive-check'
			$scope.testidhour3 = 'inactive-check'
			$scope.testidhour4 = 'active-check'

			$rootScope.searchedTutor.hrs = '2.5 Hour'
		}
	}
	var city = {}
	$scope.$on('gmPlacesAutocomplete::placeChanged', function() {
		city = $rootScope.searchedTutor.city.getPlace()
		$rootScope.searchedTutor.cityName = city.name
			// $state.go('steptwo')
		console.log($rootScope.searchedTutor.cityName)
		$scope.$apply();
	})

	$scope.disableTap = function() {
		var container = document.getElementsByClassName('pac-container');
		angular.element(container).attr('data-tap-disabled', 'true');
		var backdrop = document.getElementsByClassName('backdrop');
		angular.element(backdrop).attr('data-tap-disabled', 'true');
		angular.element(container).on("click", function() {
			document.getElementById('pac-input').blur();
		});
	};

	$scope.searchStep1 = function() {
		$state.go('findTutor3')
	}

	// $scope.searchStep2 = function(){
	//  $state.go('findTutor3')
	// }

	$scope.searchStep3 = function() {
		$state.go('findTutor4')
	}

	$scope.searchStep4 = function() {
		$state.go('findTutor5')
	}

	$scope.searchStep5 = function() {
		$state.go('findTutor6')
	}

	$scope.searchStep6 = function() {
		$ionicLoading.show()
		if ($rootScope.searchedTutor.mon_mor) { $rootScope.searchedTutor.preferredTime = 'Mon - Morning' } else if ($rootScope.searchedTutor.mon_after) { $rootScope.searchedTutor.preferredTime = 'Mon - Afternoon' } else if ($rootScope.searchedTutor.mon_eve) { $rootScope.searchedTutor.preferredTime = 'Mon - Evening' } else if ($rootScope.searchedTutor.tue_mor) { $rootScope.searchedTutor.preferredTime = 'Tue - Morning' } else if ($rootScope.searchedTutor.tue_after) { $rootScope.searchedTutor.preferredTime = 'Tue - Afternoon' } else if ($rootScope.searchedTutor.tue_eve) { $rootScope.searchedTutor.preferredTime = 'Tue - Evening' } else if ($rootScope.searchedTutor.wed_mor) { $rootScope.searchedTutor.preferredTime = 'Wed - Morning' } else if ($rootScope.searchedTutor.wed_after) { $rootScope.searchedTutor.preferredTime = 'Wed - Afternoon' } else if ($rootScope.searchedTutor.wed_eve) { $rootScope.searchedTutor.preferredTime = 'Wed - Evening' } else if ($rootScope.searchedTutor.thu_mor) { $rootScope.searchedTutor.preferredTime = 'Thu - Morning' } else if ($rootScope.searchedTutor.thu_after) { $rootScope.searchedTutor.preferredTime = 'Thu - Afternoon' } else if ($rootScope.searchedTutor.thu_eve) { $rootScope.searchedTutor.preferredTime = 'Thu - Evening' } else if ($rootScope.searchedTutor.fri_mor) { $rootScope.searchedTutor.preferredTime = 'Fri - Morning' } else if ($rootScope.searchedTutor.fri_after) { $rootScope.searchedTutor.preferredTime = 'Fri - Afternoon' } else if ($rootScope.searchedTutor.fri_eve) { $rootScope.searchedTutor.preferredTime = 'Fri - Evening' } else if ($rootScope.searchedTutor.sat_mor) { $rootScope.searchedTutor.preferredTime = 'Sat - Morning' } else if ($rootScope.searchedTutor.sat_after) { $rootScope.searchedTutor.preferredTime = 'Sat - Afternoon' } else if ($rootScope.searchedTutor.sat_eve) { $rootScope.searchedTutor.preferredTime = 'Sat - Evening' } else if ($rootScope.searchedTutor.sun_mor) { $rootScope.searchedTutor.preferredTime = 'Sun - Morning' } else if ($rootScope.searchedTutor.sun_after) { $rootScope.searchedTutor.preferredTime = 'Sun - Afternoon' } else if ($rootScope.searchedTutor.sun_eve) { $rootScope.searchedTutor.preferredTime = 'Sun - Evening' }

		if ($rootScope.loggedInUser) {
			var data = {
				request: 'tutor/search',
				authkey: $rootScope.loggedInUser.authkey,
				data: {
					// gender                  : $rootScope.searchedTutor.gender,
					subject: $rootScope.searchedTutor.subject,
					grade: $rootScope.searchedTutor.grade,
					city: $rootScope.searchedTutor.cityName,
					preferred_time: $rootScope.searchedTutor.preferredTime,
					// preferred_time       : "Mon - Morning,Mon - Afternoon,Mon - Evening",
					hours: $rootScope.searchedTutor.hrs
				}
			}
			console.log(data)
			$http.post(URL.baseDomain + "tutor/search", data)
				.success(function(success) {
					$ionicLoading.hide();
					console.log(success)
					$rootScope.selectedTutors = success.data
					if ($rootScope.selectedTutors.length) {
						for (var i in $rootScope.selectedTutors) {
							$rootScope.selectedTutors[i].img = URL.imgUrl + $rootScope.selectedTutors[i].photo
						}
						// $rootScope.selectedTutors.img = URL.imgUrl + $rootScope.selectedTutors.photo
						$rootScope.length = $rootScope.selectedTutors.length
							// console.log($rootScope.selectedTutors.photo)
						console.log($rootScope.selectedTutors)
						$state.go('tutorlist')
					} else {
						alert('No result found')
						$state.go('findTutor1')
					}
				})
				.error(function(error) {
					$ionicLoading.hide();
					console.log(error)
				})
		} else {
			$ionicLoading.hide()
			
			$ionicPopup.show({
         		buttons: [
		           {
		               	text: '<p>Login</p>',
		               	type: 'popup-close',
		               	onTap: function (e) {
		               		return 'login'
		               	}
		           },
		           {
		           		text: '<p>Sign Up</p>',
		               	type: 'popup-close',
		               	onTap: function (e) {
		               		return 'signup'
		               	}
		           }
	           	],
	         	template: 'please login or signup to continue',
	         	// title: 'Name',
	         	scope: $scope
	      	}).then(function(res) {
	         	if(res == 'login'){
	         		$state.go('login')
	         	}
	         	else{
	         		$state.go('signup1')
	         	}
	      	});
			
		}
	}

	$rootScope.getNumber = function(num) {
		if (num) {
			var num1 = parseInt(num)
			return new Array(num1);
		} else {
			return new Array(0)
		}
	}
	
	$scope.listclass = 'tutor-list'
	$scope.reverse = true;

	$scope.filterlow = function(){
		$scope.reverse = true;
	}
	
	$scope.filterhigh = function(){
		$scope.reverse = false;
	}

	$rootScope.selectTutor = function(id, img) {
		$ionicLoading.show()
		$rootScope.selectedTutorId = id
		var data = {
			request: 'maitutor/profile',
			authkey: $rootScope.loggedInUser.authkey,
			data: {
				"id": $rootScope.selectedTutorId,
			}
		}
		$http(URL.baseDomain + "maitutor/profile", data)
			.success(function(success) {
				$ionicLoading.hide();
				var race 
				var qualification 
				$rootScope.selectedTutorsProfile = success.data
				if($rootScope.selectedTutorsProfile.qualification){
					qualification = $rootScope.selectedTutorsProfile.qualification.split(",");
				}
				if($rootScope.selectedTutorsProfile.race){
					race = $rootScope.selectedTutorsProfile.race.split(",");
				}
				
				$rootScope.selectedTutorsProfile.qualificationList = qualification
				$rootScope.selectedTutorsProfile.raceList = race
				$rootScope.selectedTutorsProfile.img = img
				console.log($rootScope.selectedTutorsProfile)
				$state.go('tutorDetail')
			})
			.error(function(error) {
				$ionicLoading.hide();
				console.log(error)
			})
	}

	$rootScope.studentPicUpload = function() {
		$state.go('steptwo')
		var options = {
			destinationType: Camera.DestinationType.FILE_URI,
			sourceType: Camera.PictureSourceType.CAMERA,
			allowEdit: true,
			correctOrientation: true
		}
		$cordovaCamera.getPicture(options).then(function(imageURI) {
			// var image = document.getElementById('myImage');
			$rootScope.studentImage = imageURI;
			$state.go('steptwo')
		}, function(err) {
			alert('Failed because: ' + err);
		});
	}

	if ($rootScope.studentImage == undefined) {
		$rootScope.studentImage = 'img/profile-pic.jpg'
	}

	// $rootScope.studentPicGalary = function() {
	// 	var options = {
	// 		maximumImagesCount: 1,
	// 		width: 800,
	// 		height: 800,
	// 		quality: 80
	// 	};

	// 	$cordovaImagePicker.getPictures(options)
	// 		.then(function(results) {
	// 			$rootScope.studentImage = results[0]
	// 			$state.go('steptwo')
	// 		}, function(error) {
	// 			alert(error)
	// 		});
	// }

	$rootScope.requestTeacher = function() {
		$ionicLoading.show()
		var data = {
			"request": 'maitutor/request',
			// "authkey": '$rootScope.loggedInUser.authkey',
			"authkey": $rootScope.loggedInUser.authkey,
			"data": {
				"to": $rootScope.selectedTutorId,
				"request": {
					"subject": $rootScope.searchedTutor.subject,
					"grade": $rootScope.searchedTutor.grade,
					"hours": 1,
					"sessions": 3,
					"student_name": $rootScope.student.name,
					"student_gender": $rootScope.student.gender,
					"student_age": $rootScope.student.age,
					"address": $rootScope.student.address,
					// "city": '111111'
					"city": $rootScope.searchedTutor.cityName
				}
			}
		}
		$http.post(URL.baseDomain + "maitutor/request", data)
			.success(function(success) {
				$ionicLoading.hide();
				$rootScope.searchedTutor = {}
				$scope.hidestudentinfo = true;
				$timeout(function() {
      				$state.go('parentHome');
      			}, 2000);
			})
			.error(function(error) {
				$ionicLoading.hide();
				console.log(error)
			})
	}

	$scope.getLocation = function() {
		alert('Please turn on gps of your device')
		var geocoder
		$ionicLoading.show();
		navigator.geolocation.getCurrentPosition(successFunction, errorFunction);

		//Get the latitude and the longitude;
		function successFunction(position) {
			$ionicLoading.hide();
			var lat = position.coords.latitude;
			var lng = position.coords.longitude;
			geocoder = new google.maps.Geocoder();
			codeLatLng(lat, lng)
		}

		function errorFunction(error) {
			$ionicLoading.hide();
			alert("Geocoder failed");
			alert(JSON.stringify(error))
		}

		function codeLatLng(lat, lng) {
			var latlng = new google.maps.LatLng(lat, lng);
			geocoder.geocode({ 'latLng': latlng }, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					$ionicLoading.hide();
					if (results[1]) {
						$scope.$apply(function() {
							var add = results[0].formatted_address.split(',')
							// console.log(add[add.length-3]+','+add[add.length-2]+','+add[add.length-1])
							if($rootScope.loggedInUser){
								$rootScope.loggedInUser.address = add[add.length-3]+','+add[add.length-2]+','+add[add.length-1]								
							}
							else{
								$scope.location = add[add.length-3]+','+add[add.length-2]+','+add[add.length-1] 
							}
							
						})
						for (var i = 0; i < results[0].address_components.length; i++) {
							for (var b = 0; b < results[0].address_components[i].types.length; b++) {
								if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
									city = results[0].address_components[i];
									break;
								}
							}
						}
					} else {
						alert("No results found");
					}
				} else {
					alert("Geocoder failed due to: " + status);
				}
			})
		}
		geocoder = new google.maps.Geocoder();
	}

	// $scope.getLocation = function() {
		
	// 	function checkAvailability(){
	// 	    cordova.plugins.diagnostic.isGpsLocationAvailable(function(available){
	// 	        console.log("GPS location is " + (available ? "available" : "not available"));
	// 	        if(!available){
	// 	           checkAuthorization();
	// 	        }else{
	// 	        	mydefine();
	// 	            console.log("GPS location is ready to use");
	// 	        }
	// 	    }, function(error){
	// 	        console.error("The following error occurred: "+error);
	// 	    });
	// 	}

	// 	function checkAuthorization(){
	// 	    cordova.plugins.diagnostic.isLocationAuthorized(function(authorized){
	// 	        console.log("Location is " + (authorized ? "authorized" : "unauthorized"));
	// 	        if(authorized){
	// 	            checkDeviceSetting();
	// 	        }else{
	// 	            cordova.plugins.diagnostic.requestLocationAuthorization(function(status){
	// 	                switch(status){
	// 	                    case cordova.plugins.diagnostic.permissionStatus.GRANTED:
	// 	                        console.log("Permission granted");
	// 	                        checkDeviceSetting();
	// 	                        break;
	// 	                    case cordova.plugins.diagnostic.permissionStatus.DENIED:
	// 	                        console.log("Permission denied");
	// 	                        // User denied permission
	// 	                        break;
	// 	                    case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
	// 	                        console.log("Permission permanently denied");
	// 	                        // User denied permission permanently
	// 	                        break;
	// 	                }
	// 	            }, function(error){
	// 	                console.error(error);
	// 	            });
	// 	        }
	// 	    }, function(error){
	// 	        console.error("The following error occurred: "+error);
	// 	    });
	// 	}

	// 	function checkDeviceSetting(){
	// 	    cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled){
	// 	        console.log("GPS location setting is " + (enabled ? "enabled" : "disabled"));
	// 	        if(!enabled){
	// 	            cordova.plugins.locationAccuracy.request(function (success){
	// 	                console.log("Successfully requested high accuracy location mode: "+success.message);
	// 	            }, function onRequestFailure(error){
	// 	                console.error("Accuracy request failed: error code="+error.code+"; error message="+error.message);
	// 	                if(error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED){
	// 	                    if(confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")){
	// 	                        cordova.plugins.diagnostic.switchToLocationSettings();
	// 	                    }
	// 	                }
	// 	            }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY);
	// 	        }
	// 	    }, function(error){
	// 	        console.error("The following error occurred: "+error);
	// 	    });
	// 	}

	// 	checkAvailability();

	// 	function mydefine(){
	// 		var geocoder
	// 		$ionicLoading.show();
	// 		navigator.geolocation.getCurrentPosition(successFunction, errorFunction);

	// 		//Get the latitude and the longitude;
	// 		function successFunction(position) {
	// 			$ionicLoading.hide();
	// 			var lat = position.coords.latitude;
	// 			var lng = position.coords.longitude;
	// 			geocoder = new google.maps.Geocoder();
	// 			codeLatLng(lat, lng)
	// 		}

	// 		function errorFunction(error) {
	// 			$ionicLoading.hide();
	// 			alert("Geocoder failed");
	// 			alert(JSON.stringify(error))
	// 		}

	// 		function codeLatLng(lat, lng) {
	// 			var latlng = new google.maps.LatLng(lat, lng);
	// 			geocoder.geocode({ 'latLng': latlng }, function(results, status) {
	// 				if (status == google.maps.GeocoderStatus.OK) {
	// 					$ionicLoading.hide();
	// 					if (results[1]) {
	// 						$scope.$apply(function() {
	// 							var add = results[0].formatted_address.split(',')
	// 							// console.log(add[add.length-3]+','+add[add.length-2]+','+add[add.length-1])
	// 							if($rootScope.loggedInUser){
	// 								$rootScope.loggedInUser.address = add[add.length-3]+','+add[add.length-2]+','+add[add.length-1]								
	// 							}
	// 							else{
	// 								$scope.location = add[add.length-3]+','+add[add.length-2]+','+add[add.length-1] 
	// 							}
								
	// 						})
	// 						for (var i = 0; i < results[0].address_components.length; i++) {
	// 							for (var b = 0; b < results[0].address_components[i].types.length; b++) {
	// 								if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
	// 									city = results[0].address_components[i];
	// 									break;
	// 								}
	// 							}
	// 						}
	// 					} else {
	// 						alert("No results found");
	// 					}
	// 				} else {
	// 					alert("Geocoder failed due to: " + status);
	// 				}
	// 			})
	// 		}
	// 		geocoder = new google.maps.Geocoder();
	// 	}
	// }


	if($rootScope.fromEdit == undefined){
		$rootScope.fromEdit = false
	}
	
		if($scope.name == undefined){
			$scope.name = false
		} 
		if($scope.age == undefined){
			$scope.age = false
		} 
		if($scope.gender == undefined){
			$scope.gender = false
		} 
		if($scope.address == undefined){
			$scope.address = false
		} 
	$scope.gotoFindTutor1 = function(){
		$rootScope.fromEdit = true;
		$state.go('findTutor1')
	}
	$scope.gotoFindTutor3 = function(){
		$rootScope.fromEdit = true;
		$state.go('findTutor3')
	}
	$scope.gotoFindTutor4 = function(){
		$rootScope.fromEdit = true;
		$state.go('findTutor4')
	}
	$scope.gotoFindTutor5 = function(){
		$rootScope.fromEdit = true;
		$state.go('findTutor5')
	}
	$scope.gotoFindTutor6 = function(){
		$rootScope.fromEdit = true;
		$state.go('findTutor6')
	}
	$scope.editname = function(){
		$scope.name = true;
		$scope.age = false;
		$scope.gender = false;
		$scope.address = false;
	}
	$scope.editage = function(){
		$scope.name = false;
		$scope.age = true;
		$scope.gender = false;
		$scope.address = false;
	}
	$scope.editgender = function(){
		$scope.name = false;
		$scope.age = false;
		$scope.gender = true;
		$scope.address = false;
	}
	$scope.editaddress = function(){
		$scope.name = false;
		$scope.age = false;
		$scope.gender = false;
		$scope.address = true;
	}
	$scope.goback = function(value){

	}

	$scope.editstudentname = function(){
		$state.go('editstudentname')
	}
	$scope.editstudentage = function(){
		$state.go('editstudentage')
	}
	
	// $rootScope.popupclose = function(){
	// 	// alert(123)
	// 	$rootScope.mypopup.close()
	// }

	// $scope.editstudentage = function(){
	// 	$state.go('editstudentgender')
	// }
	$scope.studentgender = false
	$scope.malestudent = false
	$scope.femalestudent = false
	$scope.editstudentgender = function(classname){
		console.log(classname)
		if(classname){
			$scope.studentgender = false
		}
		else{
			$scope.studentgender = true
		}
	}
	$scope.ismale = function(clsname){
		$rootScope.student.gender = 'male'
		if(clsname){
			$scope.malestudent = false
			$scope.femalestudent = true
		}
		else{
			$scope.malestudent = true
			$scope.femalestudent = false
		}
		$scope.studentgender = false
	}

	$scope.isfemale = function(clsname){
		$rootScope.student.gender = 'female'
		if(clsname){
			$scope.malestudent = true
			$scope.femalestudent = false
		}
		else{
			$scope.malestudent = false
			$scope.femalestudent = true
		}
		$scope.studentgender = false
	}
	$scope.editstudentaddress = function(){
		$state.go('editstudentaddress')
	}
	$scope.popupshow = false
	
	$scope.popup = function(value){
		 
		if($scope.popupshow){
			$scope.listclass = 'tutor-list'
			$scope.popupshow = false
		}
		else{
			$scope.listclass = 'tutor-list-down'
			$scope.popupshow = true
		}
		
		// $ionicPopup.show({
  //        	template: '<ul><li>Sort tutor by</li><li>Lowest Price</li><li>Highest Price</li><li>Ratings</li></ul>',
  //        	title: '',
  //        	scope: $rootScope,
  //        	buttons: [
	 //           {
	 //               	text: 'cancle',
	 //               	type: 'common-btn',
	 //               	onTap: function (e) {
	 //               		return 'male'
	 //               	}
	 //           },
	 //           {
	 //           		text: 'ok',
	 //               	type: 'common-btn',
	 //               	onTap: function (e) {
	 //               		return 'female'
	 //               	}
	 //           }
  //          ]
  //     	})
  //     	.then(function(res) {
  //        	type: 'popup-close'
  //     	});
	}
	
	$scope.readmore = function(){
		$state.go('readmore')
	}
})
