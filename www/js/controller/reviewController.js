
maiTutor.controller('reviewController', function($scope, $rootScope, $state, $http, $ionicLoading, URL, $ionicPopup){
	if($rootScope.report == undefined){
		$rootScope.report = {}
	}

	$scope.getstudentreview = function(){
		var data = {
			"request":"user/get_students_report_card",
			"data": {
				"parent_id": $rootScope.loggedInUser.user.id
			} 
		}
		$http({
			method  : "POST",
			url     : URL.baseDomain + "user/get_students_report_card",
			data    : data 
		})
		.success(function(success){
			$scope.reports = success.reports;
			for(var i in $scope.reports){
				$scope.reports[i].request_data = JSON.parse($scope.reports[i].request_data)
				$scope.reports[i].request_data
			}
			console.log($scope.reports)
			// for(var i=1; i<$scope.reports.length; i++){
			// 	if($scope.reports[i-1].request_data.student_name == $scope.reports[i].request_data.student_name){
			// 		console.log(123)
			// 	}
			// }
			// console.log(success)
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}
	
	$scope.gotoselectsubject = function(name){
		$scope.studentname = name;
		$state.go('reportcardsubject')
		
	}

	$scope.gotoselectsession = function(report){
		$state.go('reportcardsession')
	}

	$scope.getreport = function(report){
		$rootScope.report = report
		$state.go('viewreport')

		console.log($scope.report)
		
	}
	
	$scope.addtutorreview = function(){
		var data = {
			"request":"user/add_tutor_review",
			
			"data": {
				"parent_id": 2, 
				"tutor_id": 41, 
				"job_order_id": 14, 
				"review": "bla bla", 
				"knowledge": 4, 
				"punctuality": 4, 
				"attitude": 4, 
				"communication": 4, 
				"value_for_money":5
			} 
		}

		$http({
			method  : "POST",
			url     : URL.baseDomain + "user/add_tutor_review",
			data    : data 
		})
		.success(function(success){
			alert(JSON.stringify(success))
		})
		.error(function(error){
			$ionicLoading.hide();
			alert(JSON.stringify(error))
		})
	}

	$scope.gettutorreview = function(){
		var data = {
			"request":"user/get_tutor_reviews",
			"data": {
				"tutor_id": 41
			} 
		}

		$http({
			method  : "POST",
			url     : URL.baseDomain + "user/get_tutor_reviews",
			data    : data 
		})
		.success(function(success){
			console.log(success)
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}

	$scope.getalltutorreview = function(){
		var data = {
			"request":"user/get_all_tutor_reviews"
		}

		$http({
			method  : "POST",
			url     : URL.baseDomain + "user/get_all_tutor_reviews",
			data    : data 
		})
		.success(function(success){
			console.log(success)
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}	


	
})



