maiTutor.controller('scheduleController', function($scope, $rootScope, $state, $http, $ionicLoading, URL, $ionicPopup){
	if($rootScope.selectedMyTutor == undefined){
		$rootScope.selectedMyTutor = {}
	}
	if($rootScope.upcommingschedules == undefined){
		$rootScope.upcommingschedules = []
	}
	if($rootScope.schedules == undefined){
		$rootScope.schedules = []
	}
	$scope.getScheduleList = function(){
		var data = {
			"request": 'maitutor/get_schedule',
			"authkey": $rootScope.loggedInUser.authkey,
			"data": {
  				"email": $rootScope.loggedInUser.user.email,
				"type" : "user"
			}
		}
    	console.log(data)
    	$ionicLoading.show();
		$http.post(URL.baseDomain + "maitutor/get_schedule", data)
		.success(function(success){
			$ionicLoading.hide();
			// $rootScope.schedules = success.data
			// console.log(success.data)
			var date = new Date();
			today =  date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2)
			for(var i in success.data){
				if(new Date(date) > new Date(success.data[i].date)){
					$rootScope.upcommingschedules.push(success.data[i])
				}
				else{
					$rootScope.schedules.push(success.data[i])
				}
				
				// console.log($rootScope.schedules[i].date)
			}
			console.log($rootScope.upcommingschedules)
			console.log($rootScope.schedules)
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}

	$scope.scheduleDetail = function(details){
		$rootScope.scheduleData = details;
		$state.go('scheduledetails')
		console.log(details)
	}

	$scope.myTutor = function(){
		$ionicLoading.show()
		var data = {
			"request": 'tutor/get_tutor_list',
 			"authkey": $rootScope.loggedInUser.authkey,
 			"data": {
  				"from": $rootScope.loggedInUser.user.email
 			}

		}
		$http.post(URL.baseDomain + "tutor/get_tutor_list", data)
		.success(function(success){
			$ionicLoading.hide();
			$rootScope.tutors = success.data
			console.log($rootScope.tutors)
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error)
		})
	}
	
	$scope.getMyTutorDetail = function(detail){
		$rootScope.selectedMyTutor = detail
		console.log($rootScope.selectedMyTutor)
		$state.go('myTutorDetail')
	}
	if($rootScope.count == undefined){
		$rootScope.count = {};	
	}
	
	$scope.makePay = function(){
		$rootScope.count.value = 3;
		$state.go('acceptedRequest')
	}
	
	$scope.addtimetablevalue = false
	
	$scope.addtimetable = function(){
		$scope.addtimetablevalue = true
	}
	
	$scope.sessionDate = function(){
		$scope.addtimetablevalue = false
	}
	if($rootScope.timetableselected == undefined){
		$rootScope.timetableselected = false	
	}
	if($rootScope.msgtutorselected == undefined){
		$rootScope.msgtutorselected = false	
	}
	if($rootScope.msgtutorskipped == undefined){
		$rootScope.msgtutorskipped = false	
	}
	
	$scope.skipwritemsg = function(){
		$rootScope.count.value = 1;
		$rootScope.msgtutorskipped = true
		$rootScope.msgtutorselected = false
		$state.go('acceptedRequest')
	}

	$scope.sessionDateOk = function(){
		$rootScope.count.value = 2;
		$rootScope.timetableselected = true;
		$state.go('acceptedRequest')
	}

	$scope.toacceptpage = function(){
		$rootScope.count.value = 1;
		$rootScope.msgtutorskipped = false;
		$rootScope.msgtutorselected = true;
		$state.go('acceptedRequest')
	}
	
	$scope.banklist = ["Maybank2u","CIMB Click","RHB Online","AmOnline","Hong Leong Bank Online","Public Bank","Bank Islam","Affin Bank"]
	$scope.isSelected=false
	$scope.selectbank = function(num){
		$scope.isSelected=true
		if(num == 1){
			$scope.clscolor1 = 'col-red';
			$scope.clscolor2 = '';
			$scope.clscolor3 = '';
			$scope.clscolor4 = '';
			$scope.clscolor5 = '';
			$scope.clscolor6 = '';
			$scope.clscolor7 = '';
			$scope.clscolor8 = '';
		}
		else if(num == 2){
			$scope.clscolor2 = 'col-red';
			$scope.clscolor1 = '';
			$scope.clscolor3 = '';
			$scope.clscolor4 = '';
			$scope.clscolor5 = '';
			$scope.clscolor6 = '';
			$scope.clscolor7 = '';
			$scope.clscolor8 = '';
		}
		else if(num == 3){
			$scope.clscolor3 = 'col-red';
			$scope.clscolor1 = '';
			$scope.clscolor2 = '';
			$scope.clscolor4 = '';
			$scope.clscolor5 = '';
			$scope.clscolor6 = '';
			$scope.clscolor7 = '';
			$scope.clscolor8 = '';
		}
		else if(num == 4){
			$scope.clscolor4 = 'col-red';
			$scope.clscolor1 = '';
			$scope.clscolor3 = '';
			$scope.clscolor2 = '';
			$scope.clscolor5 = '';
			$scope.clscolor6 = '';
			$scope.clscolor7 = '';
			$scope.clscolor8 = '';
		}
		else if(num == 5){
			$scope.clscolor5 = 'col-red';
			$scope.clscolor1 = '';
			$scope.clscolor3 = '';
			$scope.clscolor4 = '';
			$scope.clscolor2 = '';
			$scope.clscolor6 = '';
			$scope.clscolor7 = '';
			$scope.clscolor8 = '';
		}
		else if(num == 6){
			$scope.clscolor6 = 'col-red';
			$scope.clscolor1 = '';
			$scope.clscolor3 = '';
			$scope.clscolor4 = '';
			$scope.clscolor5 = '';
			$scope.clscolor2 = '';
			$scope.clscolor7 = '';
			$scope.clscolor8 = '';
		}
		else if(num == 7){
			$scope.clscolor7 = 'col-red';
			$scope.clscolor1 = '';
			$scope.clscolor3 = '';
			$scope.clscolor4 = '';
			$scope.clscolor5 = '';
			$scope.clscolor6 = '';
			$scope.clscolor2 = '';
			$scope.clscolor8 = '';
		}
		else if(num == 8){
			$scope.clscolor8 = 'col-red';
			$scope.clscolor1 = '';
			$scope.clscolor3 = '';
			$scope.clscolor4 = '';
			$scope.clscolor5 = '';
			$scope.clscolor6 = '';
			$scope.clscolor7 = '';
			$scope.clscolor2 = '';
		}
		// if(num)
		// 	$scope.classcolor = 'col-red'; 
		// // }
	}

})
