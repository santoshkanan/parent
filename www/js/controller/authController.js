maiTutor.controller("authController", function($scope, $rootScope, $http, $state, $ionicLoading, URL, localStorageService, $cordovaDatePicker){
	$scope.user = {};
	$scope.oldUser = {}

	$scope.user.email = 'anwar1983@gmail.com'
	$scope.user.password = '123456';
		
	if($rootScope.newUser == undefined){
		$rootScope.newUser = {};
	}
	
	if($scope.remember == undefined){
		$scope.remember = false;
	}
	if($scope.passwordType == undefined){
		$scope.passwordType = 'password'
	}

 	$scope.emailFormat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

	$scope.rememberMe = function(){
		$scope.remember = true;
	}

	$scope.login = function(){
		$ionicLoading.show();
 		// window.FirebasePlugin.getToken(function(token) {
	 		var data = {
				"request" : "user/auth",
				"data" : {
					"email" 		: $scope.user.email, 
					"password" 	: $scope.user.password,
					"token" 		: 'token'
				}
			}
			$http.post(URL.baseDomain + "user/auth", data)
			.success(function(success){
				$ionicLoading.hide();
				if(success.status == 'error'){
					$ionicLoading.hide();
					$state.go('login')
					alert(success.reason)
				}
				else{
					$rootScope.loggedInUser = success;
					$rootScope.loggedInUser.user.img = URL.imgUrl+$rootScope.loggedInUser.user.photo
					console.log($rootScope.loggedInUser)
					if($scope.remember){
						localStorageService.set('currentUser', $rootScope.loggedInUser)
					}
					$state.go('parentHome')
					var subData = {
						"request": 'maitutor/parent_notification',
						"authkey": $rootScope.loggedInUser.authkey,
	 					"data": {
	  						"user_email": $rootScope.loggedInUser.user.email
	 					}
					}
					$http({
						method	: "POST",
						url 	: URL.baseDomain + "maitutor/parent_notification",
						data	: subData 
					})
					.success(function(success){
						$ionicLoading.hide();
						$rootScope.unReadNotification = success.data
						$state.go('parentHome')
					})
					.error(function(error){
						$ionicLoading.hide();
						console.log(error)
					})
				}
			})
			.error(function(error){
				$ionicLoading.hide();
				console.log(error)
			})
		// }, 
  //     	function(error) {
  //       	$ionicLoading.hide();
  //       	alert(JSON.stringify(error));
  //     	});	
	}

	$scope.datepicker = function(){
		var options = {
		    date: new Date(),
		    mode: 'date', // or 'time'
		    maxDate: new Date() - 10000,
		    allowOldDates: true,
		    allowFutureDates: false,
		    doneButtonLabel: 'DONE',
		    doneButtonColor: '#F2F3F4',
		    cancelButtonLabel: 'CANCEL',
		    cancelButtonColor: '#000000'
		  };
		$cordovaDatePicker.show(options).then(function(date){
       		$rootScope.newUser.dob = formatDate(date)
    	});
	}

	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}

	$scope.showPassword = function(type){
		if(type == 'password'){
			$scope.passwordType = 'text'
		}
		else{
			$scope.passwordType = 'password'
		}
	}

	$scope.signup6 = function(){
		var data = {
			"request": "user/getuserdata",
			"data": {
				"mobile": $rootScope.newUser.mobile
			}
		}
		$http.post(URL.baseDomain + "user/getuserdata",data)
		.success(function(success){
			if(success.status){
				$state.go('signup6')	
			}
		})
		.error(function(error){

		})
	}
	
	$scope.signup = function(){
	    var data1 = {
			"request": "user/verify_mobile",
			"data": {
				"mobile": $rootScope.newUser.mobile,
				"code" : $rootScope.newUser.mobilecode
			}
		}
		
		$http.post(URL.baseDomain + "user/verify_mobile",data1)
		.success(function(success){
			console.log(success)
			if(success.status){
				$ionicLoading.show();
		    	var data = {
					request : 'user/signup',
					data : {
						firstname 	: $rootScope.newUser.firstname,
						lastname  	: $rootScope.newUser.lastname,
						email 		: $rootScope.newUser.email,
						password 	: $rootScope.newUser.password,
						dob 		: $rootScope.newUser.dob,
						mobile 		: $rootScope.newUser.mobile,
					}
				}
				$http.post(URL.baseDomain + "user/signup",data)
				.success(function(success){
					$ionicLoading.hide();
					if(success.status == "error"){
						alert(success.reason)
						$state.go('signup1')
					}
					else{
						$state.go('login')
						console.log(success)
					}
				})
				.error(function(error){
					$ionicLoading.hide();
					console.log(error)
				})
			}
		})
		.error(function(error){

		})
	    
	}

	$scope.forgetPassword = function(){
		$ionicLoading.show();
		var data = {
			request: 'user/pwdreset',
			data : {
				'email' : $scope.oldUser.email
			}
		}

		$http.post(URL.baseDomain + "user/pwdreset", data)
		.success(function(success){
			$ionicLoading.hide();
			alert('Password change request succesfull check your email')
			$state.go('login')
			console.log(success)
		})
		.error(function(error){
			$ionicLoading.hide();
			console.log(error);
		})
	}
})
